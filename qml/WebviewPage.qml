import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Web 0.2
import com.canonical.Oxide 1.19 as Oxide

import "components"

Page {
    id: webArchive
    anchors {
        fill: parent
        bottom: parent.bottom
    }
    width: parent.width
    height: parent.height

    header: WebViewHeader {
        title: "External Linphone Website"
        id: headerWeb
    }

    property string game

    WebContext {
        id: webcontextIF

        userScripts: Oxide.UserScript {
            context: "oxide://"
            url: Qt.resolvedUrl("js/cssinjection.js")
            matchAllFrames: true
        }
    }

    WebView {
        id: webview
        anchors {
            fill: parent
            topMargin: header.visible ? header.height : 0//test
            bottom: parent.bottom
        }
        width: parent.width
        height: parent.height

        context: webcontextIF
        url: 'https://www.linphone.org/freesip/home'
        preferences.localStorageEnabled: true
        preferences.appCacheEnabled: true
        preferences.javascriptCanAccessClipboard: true
        preferences.allowFileAccessFromFileUrls: true

        messageHandlers: [
        ]

        Component.onCompleted: {
            preferences.localStorageEnabled = true;
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: headerWeb.visible ? headerWeb.bottom : parent.top
        }
        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }
} // </page>
