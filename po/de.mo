��    I      d  a   �      0     1     :     <     >     @     B     D     F     H     J     L     N     P     R  "   T  "   w     �     �     �     �     �     �     �     �     �  
                  3     :     G     N     ]     u     y  >   �  `   �  
   %  	   0     :     R     X     `     n     �     �     �     �     �     �     �     �     �     �  I   �     G	     T	     h	     �	     �	     �	     �	  )   �	     �	     �	     	
     
     3
  4   R
     �
     �
  ,   �
  \  �
  	   -     7     9     ;     =     ?     A     C     E     G     I     K     M     O     Q  "   p     �     �     �     �  $   �     �                 	   /     9     A  	   U     _     o     x     �  
   �     �  D   �  w        |     �      �     �     �     �     �     �     �          !     .     7     @     _     h     x  G   �  
   �     �     �               "  (   5  .   ^     �     �     �     �  %   �  9        @     T  5   \        1   /   (                 B   E          :          D           *   C      +   ?                 !   <   $   7   2   #            H                        '   )                    	   
                  =   >   4   %   &      3       -   ;               0   A   9       ,      "              .   I   @   F      5             8           6           G     seconds # * + 0 1 2 3 4 5 6 7 8 9 <b>Password</b> of the SIP account <b>Username</b> of the SIP account About Add Add Contact Add Favorite Contact Add a known SIP domain: App Development Calling: Cancel Code Used from Connecting Contact Contact <b>Name</b> Create Current Call Delete Delete Contact Delete favorite contact Dev Development Do you have an existing SIP account? You can log in to use it. Do you want to create a new SIP account with Linphone? Tap to open Linphone website and sign up. Duration:  First Run Full <b>SIP Address</b> Icons In Call Incoming Call Incoming Call from URL Info Information Keep display on License GPLv2 Linphone Log in Log in with Existing Account Log out Logged in as: Login Provider <b>Domain</b>. Usually, what it goes after @ of your SIP address SIP Accounts SIP address to call Send a command to Linphone Settings Sign up Special Thanks to Stop Linphone when close Ubuntu Touch Linphone build modified code Under License %1 Use dark theme Version %1. Source %2 Welcome to Linphone Will not run in the background for his untiringly testing the alpha and beta builds for starting it all offline or create a free Linphone SIP account online Project-Id-Version: linphone
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-08-12 16:42+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de
X-Poedit-Bookmarks: -1,-1,21,-1,-1,-1,-1,-1,-1,-1
  Sekunden # * + 0 1 2 3 4 5 6 7 8 9 <b>Passwort</b> des SIP Kontos <b>Benutzername</b> des SIP Kontos Über Hinzufügen Kontakt hinzufügen Favoritenkontakt hinzufügen Füge eine bekannt SIP Domain hinzu: App Entwicklung Anruf: Beenden Verwendeter Code von Verbinden Kontakt Kontakt <b>Name</b> Erstellen Aktueller Anruf Löschen Kontakt löschen Favorisierten Kontakt löschen Entwickler Entwicklung Haben Sie bereits ein SIP Konto? Melden Sie sich an um es zu nutzen. Möchten Sie ein SIP Konto mit Linphone erstellen? Klicken Sie um die Linphone Webseite zu öffnen und sich anzumelden. Dauer:  Erster Start Vollständige <b>SIP Adresse</b> Icons Im gespräch Eingehender Anruf Eingehender Anruf von URL Info Information Bildschirm angeschaltet lassen Lizenz GPLv2 Linphone Anmelden Mit bestehenden Konto anmelden Abmelden Angemeldet als: Anmelden Provider <b>Domain</b>. In der Regel, der Adressteil nach dem @ Zeichen SIP Konten SIP Adresse zum anrufen Sende einen Befehl an Linphone Einstellungen Anmelden Spezieller Dank an Beende Linphone wenn es geschlossen wird Ubuntu Touch Linphone build modifizierter Code Unter Lizenz %1 Benutze Dark Theme Version %1. Quellcode %2 Willkommen zu Linphone Wird nicht im Hintergrund ausgeführt für sein unermüdliches testen der Alpha und Beta Builds um alles zu starten offline oder erstelle online kostenlos ein Linphone SIP Konto 